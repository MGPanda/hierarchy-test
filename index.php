<?php

// Incluímos Trabajador y Empresa ya que vamos a utilizar ambos para finalizar el ejercicio.
include_once 'Trabajador.php';
include_once 'Empresa.php';

/**
 * Creamos tres trabajadores. Recuerda que aquí estamos introduciendo tres variables
 * correspondientes a Persona y dos de Trabajador, que acaban juntándose en un único bloque
 * conjunto gracias a la herencia.
 */
$pepe = new Trabajador("Pepe", 30, "Calzadilla de los Barros", "electricista", 1400);
$juan = new Trabajador("Juan", 27, "Puebloperdido de Allá", "cocinero", 1900);
$din = new Trabajador("Din Djarin", 43, "Nevarro", "cazarrecompensas", 0.5);

/**
 * Creamos una empresa y utilizamos los métodos creados para añadir a los tres trabajadores
 * creados anteriormente. Finalmente, hacemos que todos ellos se introduzcan mediante el
 * método "presentarEmpresa()".
 */
$ecorp = new Empresa("E Corp", "22111963X", "Génova");

$ecorp->addTrabajador($pepe);
$ecorp->addTrabajador($juan);
$ecorp->addTrabajador($din);

$ecorp->presentarEmpresa();
