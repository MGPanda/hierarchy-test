<?php

/**
 * Creamos una clase Persona que contiene toda la información correspondiente. La hacemos "abstracta" ya
 * que en ningún momento vamos a querer crear una persona como tal, si no elementos derivados de Persona
 * como puede ser Trabajador.
 */
abstract class Persona
{
    // Le damos tres variables propias de cualquier persona.
    var $nombre;
    var $edad;
    var $localidad;

    /**
     * Creamos el constructor y asignamos las variables pasadas como parámetros. La hacemos protected ya
     * que nos interesará utilizarlo después en las clases hijas.
     */
    protected function __construct($nombre, $edad, $localidad)
    {
        $this->nombre = $nombre;
        $this->edad = $edad;
        $this->localidad = $localidad;
    }

    /**
     * Creamos una función para presentarse. También la hacemos "protected", ya que nos interesa que 
     * sea quien sea a quien creemos sepa cómo introducirse al público.
     */
    protected function presentarse()
    {
        echo "¡Hola! Me llamo $this->nombre y soy de $this->localidad.";
    }

    /**
     * Como dice el texto del interior, es imposible que este método se ejecute. Por una parte, al ser una
     * clase abstracta, no hay manera alguna de utilizar sus funciones internas (es decir, tampoco podríamos,
     * por ejemplo, hacer $persona->presentarse()), y además al ser un método privado, sus hijos no lo
     * heredarán.
     */
    private function comprobarSiEsPersona()
    {
        echo "Es imposible ejecutar este método.";
    }
}
