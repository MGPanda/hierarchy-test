<?php

/**
 * Antes de crear la clase, informamos a PHP de que vamos a requerir el archivo de clase Persona.
 * Utilizamos específicamente require y no include porque una no puede existir sin la otra.
 */
require_once 'Persona.php';

/**
 * Añadimos "extends Persona" a la clase Trabajador, haciéndola así hija de esta y heredando todas
 * sus propiedades.
 */
class Trabajador extends Persona
{
    /**
     * Creamos las variables propias de un trabajador, como pueden ser posición y salario.
     * -IMPORTANTE-: a pesar de no estar listadas aquí, las variables nombre, edad y localidad
     * existen en la clase Trabajador, ya que son heredadas de Persona. Esto significa que,
     * aunque no las declaremos aquí arriba, si por ejemplo ejecutamos un 
     * 
     * echo $this->nombre;
     * 
     * nos mostraría el nombre del trabajador por pantalla.
     */
    var $posicion;
    var $salario;

    /**
     * Creamos el constructor. En este caso le vamos a pedir al usuario que, además de las tres
     * variables propias de una Persona, nos introduzca las dos nuevas que hemos creado 
     * específicamente para Trabajador.
     * 
     * Estas dos las asignaremos normalmente, pero para asignar las tres primeras podemos reutilizar
     * el constructor de Persona. Para ello, simplemente escribimos parent::<NOMBRE DEL MÉTODO>, y
     * todo ello pasándole los parámetros necesarios. Para entendernos, el código de abajo es igual que:
     * 
     * $this->nombre = $nombre;
     * $this->edad = $edad;
     * $this->localidad = $localidad;
     * $this->posicion = $posicion;
     * $this->salario = $salario;
     */
    function __construct($nombre, $edad, $localidad, $posicion, $salario)
    {
        parent::__construct($nombre, $edad, $localidad);
        $this->posicion = $posicion;
        $this->salario = $salario;
    }

    /**
     * Al igual que con el constructor, volvemos a reutilizar el código de Persona en el método
     * presentarse. De esta manera, cuando llamemos a $trabajador->presentarse(), primero hará
     * la presentación estándar de una persona ("me llamo tal, tengo tal edad y soy de tal"),
     * y luego escribirá una segunda línea dando detalles sobre su puesto de trabajo.
     * 
     * La hacemos pública ya que nos interesa que pueda llamarse desde fuera de esta clase,
     * como desde el índice o desde una empresa, por ejemplo.
     * NOTA: por defecto, los métodos y funciones son públicos, es decir, funcionaría igual
     * si no escribiéramos la palabra "public".
     */
    public function presentarse()
    {
        parent::presentarse();
        echo "<br>Además, soy un $this->posicion que cobra $this->salario € mensuales.";
    }
}
