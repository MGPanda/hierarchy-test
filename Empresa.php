<?php

include_once 'Trabajador.php';

/**
 * Creamos una última clase llamada Empresa en la cual guardaremos nuestros trabajadores. En esta
 * situación concreta, diremos que una empresa tiene nombre, CIF, sede y una lista de trabajadores
 * como datos clave. No utilizamos extends ni implements ya que no nos interesa que Empresa herede
 * de ninguna de nuestras otras clases.
 */
class Empresa
{
    var $nombre;
    var $cif;
    var $sede;

    /**
     * En PHP no suele hacer falta inicializar variables, pero sí con las array, o si no cosas como
     * array_push nos darán error.
     */
    var $trabajadores = [];

    /**
     * Nótese cómo no pedimos al usuario una lista de trabajadores al llamar al constructor; esto es
     * porque nos interesa introducirlos de uno en uno con otro método.
     */
    function __construct($nombre, $cif, $sede)
    {
        $this->nombre = $nombre;
        $this->cif = $cif;
        $this->sede = $sede;
    }

    // addTrabajador no hace más que añadir el trabajador pasado como parámetro a la lista de la empresa.
    function addTrabajador($trabajador)
    {
        array_push($this->trabajadores, $trabajador);
    }

    /**
     * En esta función se muestra en un h1 el nombre, CIF y sede de la empresa, seguido de la presentación
     * individual de cada trabajador, mediante un foreach que llama a la función "presentarse()" de cada
     * trabajador.
     */
    function presentarEmpresa()
    {
        echo "<h1>$this->nombre, con CIF $this->cif y sede en $this->sede:</h1><hr>";
        foreach ($this->trabajadores as $trabajador) {
            $trabajador->presentarse();
            echo "<hr>";
        }
    }
}
